msgid ""
msgstr ""
"Project-Id-Version: showerloopcalculator\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-22 07:02+0200\n"
"PO-Revision-Date: 2018-10-22 05:06\n"
"Last-Translator: kepon85 <david@mercereau.info>\n"
"Language-Team: Afar\n"
"Language: aa_ER\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: showerloopcalculator\n"
"X-Crowdin-Language: aa\n"
"X-Crowdin-File: messages.pot\n"

#: Calculator.php:14
msgid "Liquid freshwater represents <a href=\"http://www.soiron.fr/rubrique.php?id_rubrique=456\" target=\"_blank\">1%</a> of all the water on Earth.  On average, in a french household, <a href=\"https://www.cieau.com/le-metier-de-leau/ressource-en-eau-eau-potable-eaux-usees/quels-sont-les-usages-domestiques-de-leau/\" target=\"_blank\">39%</a> of this water is used for bathing and showering and hot water represents <a href=\"https://www.direct-energie.com/particuliers/parlons-energie/dossiers-energie/economie-d-energie/la-consommation-electrique-du-chauffe-eau\" target=\"_blank\">15%</a> of the electricity bill. It is therefore judicious to think about ways to reduce this consumption. The <a href=\"http://showerloop.org\" target=\"_blank\">showerloop</a> is a water recycling shower : water is in a closed loop, it fall again on my head once filtered and therefore clean. It is adequate in terms of water and energy savings. The following calculator will illustrate this."
msgstr "crwdns1816:0crwdne1816:0"

#: Calculator.php:40 Calculator.php:67
msgid "Select your country's configuration"
msgstr "crwdns1818:0crwdne1818:0"

#: Calculator.php:55 Calculator.php:62
msgid "Contribute by adding your country's configuration"
msgstr "crwdns1820:0crwdne1820:0"

#: Calculator.php:56 Calculator.php:63
msgid "Change this country's configuration"
msgstr "crwdns1822:0crwdne1822:0"

#: Calculator.php:64
msgid "Your email"
msgstr "crwdns1824:0crwdne1824:0"

#: Calculator.php:81
msgid "Please indicate all or part of the information you want to add / modify\n\n"
"Country name :\n"
"Country : \n"
"WaterCostShop :\n"
"Water price :\n"
"Wastewater price :\n"
"Cold water supply temperature :\n"
"Avenage number of person in household :\n"
"Avenage number of showers per person per week :\n"
"Average time spent under the shower per person :\n"
"Average showering water temperature :\n"
"Avenage flow rate of your shower :\n"
"Most common hot water system :\n\n"
"We will process and integrate them as soon as possible,\n"
"Thank you for your contribution"
msgstr "crwdns1826:0crwdne1826:0"

#: Calculator.php:99
msgid "Suggest / Contribute"
msgstr "crwdns1828:0crwdne1828:0"

#: Calculator.php:105
msgid "Choose the level of detail"
msgstr "crwdns1830:0crwdne1830:0"

#: Calculator.php:107
msgid "Minimum"
msgstr "crwdns1832:0crwdne1832:0"

#: Calculator.php:108
msgid "Intermediate"
msgstr "crwdns1834:0crwdne1834:0"

#: Calculator.php:109
msgid "Maximum"
msgstr "crwdns1836:0crwdne1836:0"

#: Calculator.php:115
msgid "Data"
msgstr "crwdns1838:0crwdne1838:0"

#: Calculator.php:118
msgid "Energy required to raise 1L of water by 1°C"
msgstr "crwdns1840:0crwdne1840:0"

#: Calculator.php:123
msgid "Water heater efficiency (heat losses)"
msgstr "crwdns1842:0crwdne1842:0"

#: Calculator.php:128
msgid "I<span id=\"EnergyTheoretical1l1degForShower\">?</span>Wh"
msgstr "crwdns1844:0crwdne1844:0"

#: Calculator.php:132
msgid "Cold water supply temperature"
msgstr "crwdns1846:0crwdne1846:0"

#: Calculator.php:137
msgid "Water price"
msgstr "crwdns1848:0crwdne1848:0"

#: Calculator.php:142
msgid "Wastewater price"
msgstr "crwdns1850:0crwdne1850:0"

#: Calculator.php:146
msgid "Water total cost :"
msgstr "crwdns1852:0crwdne1852:0"

#: Calculator.php:151
msgid "Description of your household"
msgstr "crwdns1854:0crwdne1854:0"

#: Calculator.php:155 Calculator.php:160
msgid "Number of persons in the household"
msgstr "crwdns1856:0crwdne1856:0"

#: Calculator.php:165
msgid "Average time spent under the shower per person"
msgstr "crwdns1858:0crwdne1858:0"

#: Calculator.php:170
msgid "Average showering water temperature"
msgstr "crwdns1860:0crwdne1860:0"

#: Calculator.php:175
msgid "You're current water heating system : "
msgstr "crwdns1862:0crwdne1862:0"

#: Calculator.php:194
msgid "The cost of this energy is"
msgstr "crwdns1864:0crwdne1864:0"

#: Calculator.php:199
msgid "With your current shower"
msgstr "crwdns1866:0crwdne1866:0"

#: Calculator.php:202
msgid "Flow rate of your shower :"
msgstr "crwdns1868:0crwdne1868:0"

#: Calculator.php:203
msgid "8L/min for an eco, 20L/min for a regular showerhead (<a href=\\\"https://www.jeconomiseleau.org/index.php/particuliers/economies-par-usage/la-douche-et-le-bain\\\">source</a>)"
msgstr "crwdns1870:0crwdne1870:0"

#: Calculator.php:206
msgid "You’re using <b><span id=\"WaterPerShower\">?</span>L of water</b> per shower, meaning"
msgstr "crwdns1872:0crwdne1872:0"

#: Calculator.php:207
msgid "You need <b><span id=\"WattPerShower\">?</span>kWh of energy</b> per shower, meaning"
msgstr "crwdns1874:0crwdne1874:0"

#: Calculator.php:208
msgid "One shower cost you"
msgstr "crwdns1876:0crwdne1876:0"

#: Calculator.php:210 Calculator.php:241
msgid "Yearly, for your household"
msgstr "crwdns1878:0crwdne1878:0"

#: Calculator.php:211
msgid "You’re using <b><span id=\"WaterShowerPerYear\">?</span>L of water</b> to shower, meaning"
msgstr "crwdns1880:0crwdne1880:0"

#: Calculator.php:212
msgid "You need <b><span id=\"WattShowerPerYear\">?</span>kWh of energy</b>, meaning"
msgstr "crwdns1882:0crwdne1882:0"

#: Calculator.php:213
msgid "You have an annuel budget estimated to be around "
msgstr "crwdns1884:0crwdne1884:0"

#: Calculator.php:218
msgid "If you had a showerloop"
msgstr "crwdns1886:0crwdne1886:0"

#: Calculator.php:220
msgid "You would have the model"
msgstr "crwdns1888:0crwdne1888:0"

#: Calculator.php:228
msgid "Total cost of the showerloop is"
msgstr "crwdns1890:0crwdne1890:0"

#: Calculator.php:228
msgid "Maintenance annual cost (replacement of wearing parts) is"
msgstr "crwdns1892:0crwdne1892:0"

#: Calculator.php:233
msgid "This model of showerloop uses <b><span id=\"WaterPerShowerloop\">?</span>L of water</b>, meaning"
msgstr "crwdns1894:0crwdne1894:0"

#: Calculator.php:234
#, php-format
msgid "You’re saving <b><span id=\"WaterPerShowerCompare\">?</span>% of water</b> compared to a typical shower"
msgstr "crwdns1896:0crwdne1896:0"

#: Calculator.php:235
msgid "You would need <span id=\"WattPerShowerloopHotWater\">?</span>kWh of energy to heat the water."
msgstr "crwdns1898:0crwdne1898:0"

#: Calculator.php:236
msgid "You would need <span id=\"WattPerShowerloopOperation\">?</span>kWh of energy to run the showerloop."
msgstr "crwdns1900:0crwdne1900:0"

#: Calculator.php:237
msgid "You would need <b><span id=\"WattPerShowerloopTotal\">?</span>kWh of energy</b> per shower meaning"
msgstr "crwdns1902:0crwdne1902:0"

#: Calculator.php:238
msgid "You would save <b><span id=\"WattPerShowerCompare\">?</span> of energy</b> compared to a typical shower"
msgstr "crwdns1904:0crwdne1904:0"

#: Calculator.php:239
msgid "A shower would cost you"
msgstr "crwdns1906:0crwdne1906:0"

#: Calculator.php:242
msgid "You would use <b><span id=\"WaterShowerloopPerYear\">?</span>L of water</b> per year to shower, meaning"
msgstr "crwdns1908:0crwdne1908:0"

#: Calculator.php:243
msgid "You need <b><span id=\"WattShowerloopPerYear\">?</span>kWh of energy</b> to shower per year to shower, meaning"
msgstr "crwdns1910:0crwdne1910:0"

#: Calculator.php:244
msgid "You have an annuel budget estimated to be around"
msgstr "crwdns1912:0crwdne1912:0"

#: Calculator.php:245
msgid "The purchase of the equipment will pay for itself in <span id=\"roi\">?</span> year"
msgstr "crwdns1914:0crwdne1914:0"

#: Calculator.php:259 Calculator.php:310
msgid "Showerloop"
msgstr "crwdns1916:0crwdne1916:0"

#: Calculator.php:265 Calculator.php:316 Calculator.php:366
msgid "Typical shower"
msgstr "crwdns1918:0crwdne1918:0"

#: Calculator.php:278
msgid "The purchase of the equipment will pay for itself in"
msgstr "crwdns1920:0crwdne1920:0"

#: Calculator.php:292 Calculator.php:343 Calculator.php:385
msgid "Years"
msgstr "crwdns1922:0crwdne1922:0"

#: Calculator.php:329
msgid "Total energy consumption for the household per year for showering"
msgstr "crwdns1924:0crwdne1924:0"

#: Calculator.php:360
msgid "Showerloop (1)"
msgstr "crwdns1926:0crwdne1926:0"

#: Calculator.php:379
msgid "Shower budget for the household per year"
msgstr "crwdns1928:0crwdne1928:0"

#: Calculator.php:393
msgid "(1) Showerloop with investment and maintenance cost"
msgstr "crwdns1930:0crwdne1930:0"

#: Calculator.php:396
msgid "Share this results"
msgstr "crwdns1932:0crwdne1932:0"

#: index.php:91
msgid "Showerloop Calculator"
msgstr "crwdns1934:0crwdne1934:0"

#: index.php:132
msgid "partial translation"
msgstr "crwdns1936:0crwdne1936:0"

#: index.php:224
msgid "Showerloop calculator"
msgstr "crwdns1938:0crwdne1938:0"

#: index.php:233
msgid "Thanks to"
msgstr "crwdns1940:0crwdne1940:0"

#: index.php:234
msgid "for this <a target=\"_blank\" href=\"https://crwd.in/calcpvautonome\">translation</a>"
msgstr "crwdns1942:0crwdne1942:0"

#: index.php:236
msgid "<p>Go to the <a href=\"https://crwd.in/calcpvautonome\" target=\"_blank\">colaborative translation platform</a> to help us translate this free software.</p>"
msgstr "crwdns1944:0crwdne1944:0"

#: index.php:238
msgid "version"
msgstr "crwdns1946:0crwdne1946:0"

#: index.php:238
msgid "is an open software licensed <a href=\"https://en.wikipedia.org/wiki/Beerware\">Beerware</a>"
msgstr "crwdns1948:0crwdne1948:0"

#: index.php:238
msgid "Source"
msgstr "crwdns1950:0crwdne1950:0"

#: index.php:239
msgid "By"
msgstr "crwdns1952:0crwdne1952:0"

