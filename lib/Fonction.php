<?php

// Formulaire afficher ce qui est en get ou ce qui est dans la config
function valeurRecup($nom) {
	global $config_ini, $config_ini_default;
	if (isset($_GET[$nom])) {
		echo $_GET[$nom]; 
	} else if (isset($config_ini['form'][$nom])) {
		echo $config_ini['form'][$nom];
	} else if (isset($config_ini_default['form'][$nom])) {
		echo $config_ini_default['form'][$nom];
	} else {
		echo '';
	}
}
function valeurRecupCookie($nom) {
	global $config_ini, $config_ini_default;
	if (isset($_GET[$nom])) {
		echo $_GET[$nom]; 
	} else if (isset($_COOKIE[$nom])) {
		echo $_COOKIE[$nom];
	} else if (isset($config_ini['form'][$nom])) {
		echo $config_ini['form'][$nom];
	} else if (isset($config_ini_default['form'][$nom])) {
		echo $config_ini_default['form'][$nom];
	} else {
		echo '';
	}
}
function valeurRecupCookieSansConfig($nom) {
	global $config_ini, $config_ini_default;
	if (isset($_GET[$nom])) {
		echo $_GET[$nom]; 
	} else if (isset($_COOKIE[$nom])) {
		echo $_COOKIE[$nom];
	} else {
		echo '';
	}
}

// Forumaire sur les select mettre le "selected" au bon endroit selon le get ou la config
function valeurRecupSelect($nom, $valeur) {
	global $config_ini, $config_ini_default;
	if ($_GET[$nom] == $valeur) {
		echo ' selected="selected"'; 
	} else if (empty($_GET[$nom]) && $config_ini['form'][$nom] == $valeur) {
		echo ' selected="selected"'; 
	} else if (empty($_GET[$nom]) && $config_ini_default['form'][$nom] == $valeur) {
		echo ' selected="selected"'; 
	} else {
		echo '';
	}
}

// Récupérer l'IP
function get_ip() {
	// IP si internet partagé
	if (isset($_SERVER['HTTP_CLIENT_IP'])) {
		return $_SERVER['HTTP_CLIENT_IP'];
	}
	// IP derrière un proxy
	elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	// Sinon : IP normale
	else {
		return (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '');
	}
}

// Ajoute la langue à une URL qui n'en a pas
function addLang2url($lang) {
	global $_SERVER;
	$URIexplode=explode('?', $_SERVER['REQUEST_URI']);
	if ($URIexplode[1] != '') {
		return $URIexplode[0].$lang.'?'.$URIexplode[1];
	} else {
		return $URIexplode[0].$lang;
	}
}
function replaceLang2url($lang) {
	global $_SERVER;
	$URIexplode=explode('?', $_SERVER['REQUEST_URI']);
	$debutUrl=substr($URIexplode[0], 0, -langCountChar($URIexplode[0]));
	if ($URIexplode[1] != '') {
		return $debutUrl.$lang.'?'.$URIexplode[1];
	} else {
		return $debutUrl.$lang;
	}
}
function langCountChar($url) {
	// $url reçu c'est l'URL avant la query : ?machin=1
	if (preg_match('#/sr-Cyrl-ME$#',$url)) {
		return 10;
	} elseif (preg_match('#/[a-z]{2}-[A-Z]{2}$#',$url)) {
		return 5;
	} elseif (preg_match('#/[a-z]{3}-[A-Z]{2}$#',$url)) {
		return 6;
	} elseif (preg_match('#/[a-z]{3}$#',$url)) {
		return 3;
	} elseif (preg_match('#/[a-z]{2}$#',$url)) {
		return 2;
	}
}

?>
